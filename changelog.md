# ChangeLog

## Version DEV (??)
* Improved : 
* Modified :
* Solved :

## Version 1.0.4 (Fri, December 02 2016)
* Improved : Adding composer.json and uploaded on Packagelist

## Version 1.0.3 (Sun, November 27 2016)
* Improved : Errors in comments
* Modified : debugMail() renamed for debug()

## Version 1.0.2 (Thu, November 24 2016)
* Solved : html content displayed correctly on Gmail website

## Version 1.0.1 (Thu, November 24 2016)
* Improved : Test file improved with different types of files

## Version 1.0.0 (Thu, November 24 2016)
* Initial public release
