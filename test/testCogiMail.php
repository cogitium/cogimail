<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Test page for CogiMail Class</title>
</head>

<body>
    <div class="container">
        <h2>Test page for CogiMail</h2>
        <div>
        <?php
            require_once("../class/CogiMail.class.php");

            try {

                /**
                 * Initialize the email and the sender detail
                 */
                $mail = new CogiMail("info@cogitium.com","The sender user","phileas6677@gmail.com");

                /**
                 * Use the setters
                 */
                $mail->addRecipientEmail('info@jechercheuntoit.com');
                $mail->addRecipientEmail('phgphotographe@gmail.com');
                $mail->addBccRecipientMail('philippe.giraud@eemi.com');
                $mail->addBccRecipientMail('phileas6677@gmail.com');
                $mail->addFile('image1.jpg');
                $mail->addFile('image2.gif');
                $mail->addFile('image3.png');
                $mail->addFile('image4.jpeg');
                $mail->addFile('fichier1.pdf');
                $mail->addFile('fichier2.doc');

                /**
                 * Initialize the content
                 */
                $mailObject = 'Object of the email';
             
                $plainTextContent = 'Example,'."\n".'With only plain text'; 

                $htmlContent =  '<html> 
                                    <body>
                                        <h1>Example</h1>
                                        <p>
                                            A text with a  
                                            <strong>HTML</strong> 
                                            content
                                        </p>
                                    </body> 
                                </html>'; 

                $mail->contentMail($mailObject, $plainTextContent, $htmlContent);

                /**
                 * Send the mail
                 */
                $mail->sendMail();

                echo "<p>Very good, the email was sent !</p>";
                $mail->debug();

            } catch(Exception $e) {

                echo '<p>Exception : ' . $e->getMessage() . '</p>';
                $mail->debug();
            }
        ?>
        </div>
    </div>      
</body>
</html>
