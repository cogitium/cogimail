# CogiMail - A simple-featured email creation and transfer class for PHP #

## Class Features

- A simple code for sending email from PHP
- Send emails with multiple TOs, CCs, BCCs
- Send emails with personal sender informations and specific REPLY-TO
- Multipart/alternative emails for mail clients that do not read HTML email
- Possible to join many files to the email
- Error messages in english only
- Generate exception if a trouble appears
- Compatible with PHP 5.0 and later

## Why you might need it

Many PHP developers use email in their code. The only PHP function that supports this is the `mail()` function. However, it does not provide any assistance for making use of popular features such as HTML-based emails and attachments.

Formatting email correctly is surprisingly difficult. There are myriad overlapping RFCs, requiring tight adherence to horribly complicated formatting and encoding rules - the vast majority of code that you'll find online that uses the `mail()` function directly is just plain wrong.
*Please* don't be tempted to do it yourself - if you don't use CogiMail, there are many other excellent libraries that you should look at before rolling your own - try PhpMailer, SwiftMailer, Zend_Mail, eZcomponents etc.

In this version, you need to have a SMTP local server to use the component.

## License

Coming soon.

## Installation & loading

At the moment, you just have to copy the class file in the folder you want.

### What's included

Within the download you'll find the following directories and files. You'll see something like this:

```
cogimail/
├── bat/
│   ├── checkstyle.bat
│   ├── phpdoc.bat
├── class/
│   └── CogiMail.class.js
├── doc/
│   ├── ...
│   └── index.html
├── psr/
│   ├── ...
│   └── index.html
└── test/
    ├── testCogiMail.php
    ├── image1.jpg
    ├── image2.gif
    ├── image3.png
    ├── image4.jpeg
    ├── fichier1.pdf
    └── fichier2.doc
```

## A Simple Example

```php
<?php
    require_once("../class/CogiMail.class.php");
    try {
        /**
         * Initialize the email and the sender detail
         */
        $mail = new CogiMail("sender@mail.com","Sender Name","replyto@mail.com");

        /**
         * Use the setters
         */
        $mail->addRecipientEmail('recipient1@mail.com');
        $mail->addRecipientEmail('recipient2@mail.com');
        $mail->addBccRecipientMail('bcc_recipient1@mail.com');
        $mail->addBccRecipientMail('bcc_recipient2@mail.com');
        $mail->addFile('file1.jpg');
        $mail->addFile('file2.gif');
        $mail->addFile('file3.png');
        $mail->addFile('file4.jpeg');
        $mail->addFile('file5.pdf');
        $mail->addFile('file6.doc');

        /**
         * Initialize the content
         */
        $mailObject = 'Object of the email';
     
        $plainTextContent = 'Example,'."\n\n".'With only plain text'; 

        $htmlContent =  '<html> 
                            <body>
                                <h1>Example</h1>
                                <p>
                                    A text with a  
                                    <strong>HTML</strong> 
                                    content
                                </p>
                            </body> 
                        </html>'; 

        $mail->contentMail($mailObject, $plainTextContent, $htmlContent);

        /**
         * Send the mail
         */
        $mail->sendMail();

        echo "<p>Very good, the email was sent !</p>";
        $mail->debug();

    } catch(Exception $e) {

        echo '<p>Exception' . $e->getMessage() . '</p>';
        $mail->debug();
    }
?>
```

That's it. You should now be ready to use CogiMail ! 

## Localization

English only at this time.

## Documentation

The complete documentation is in the doc folder.

## Tests

There is a test page in the test folder to try the component.

## Contributing

An idea or a comment, please email to [Philippe](mailto:phgiraud@cogitium.com)

## Changelog

See [changelog](changelog.md).

## History

- At the beginning, CogiMail was a procedural function in a PHP library.
- Then, this function became a Class.
- Now, CogiMail is in a BitBucket repository.
- With Readme, and a PhpDocumentor folder.
- And Cherry on the Cake, there is a PSR folder to see the quality of the code ;-)